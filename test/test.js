/* eslint-disable @typescript-eslint/no-var-requires */
const test = require('tape');
const { helloWorld, helloName } = require('../dist/cjs/index.js');

test('helloWorld', (t) => {
  const result = helloWorld();
  t.equal(result, 'hello world!');
  t.end();
});

test('helloName', (t) => {
  const result = helloName('Jerry Seinfeld');
  t.equal(result, 'hello, Jerry Seinfeld');
  t.end();
});
