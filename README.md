# `ts-library-starter`

A template for starting a commonjs library written in Typescript, with linting, tests, and build processes. Includes CI configuration for Gitlab.

## CI

Configured to work with Gitlab CI.

Stages:

- Lint
- Build
- Test

## Developing

### Linting & Formatting

```
npm run lint
```

Uses ESLint with Typescript default settings.

### Tests

Tests are run on the built package.

```
npm run test
```

### Build

```
npm run build
```

The package builds to two versions:

- CommonJS: `./dist/cjs`
- ES2020 Module: `./dist/esm`

Both target ES2015.

The package.json includes a `main` and `module` property.
