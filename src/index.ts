import { helloName } from './helloName.js';
const helloWorld = (): string => 'hello world!';

export { helloName, helloWorld };
